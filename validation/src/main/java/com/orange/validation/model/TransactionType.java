package com.orange.validation.model;

import java.io.Serializable;

public enum TransactionType implements Serializable {

    IBAN_TO_IBAN,
    IBAN_TO_WALLET,
    WALLET_TO_IBAN,
    WALLET_TO_WALLET
}
