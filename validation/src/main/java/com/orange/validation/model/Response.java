package com.orange.validation.model;

import com.orange.validation.error.Error;
import java.util.HashSet;
import java.util.Set;

public class Response {

    private Set<Error> errors = new HashSet<>();

    public Set<Error> getErrors() {
        return errors;
    }

    public void setErrors(Set<Error> errors) {
        this.errors = errors;
    }
}
