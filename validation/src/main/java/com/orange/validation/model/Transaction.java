package com.orange.validation.model;

public class Transaction {

    private TransactionType transactionType;
    private String senderIban;
    private String senderCnp;
    private String receiverIban;
    private String receiverCnp;
    private String name;
    private String description;
    private double amount;

    public String getReceiverIban() {
        return receiverIban;
    }

    public void setReceiverIban(String receiverIban) {
        this.receiverIban = receiverIban;
    }

    public String getReceiverCnp() {
        return receiverCnp;
    }

    public void setReceiverCnp(String receiverCnp) {
        this.receiverCnp = receiverCnp;
    }

    public String getSenderIban() {
        return senderIban;
    }

    public void setSenderIban(String senderIban) {
        this.senderIban = senderIban;
    }

    public String getSenderCnp() {
        return senderCnp;
    }

    public void setSenderCnp(String senderCnp) {
        this.senderCnp = senderCnp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
