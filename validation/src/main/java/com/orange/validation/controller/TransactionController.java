package com.orange.validation.controller;

import com.orange.validation.model.Response;
import com.orange.validation.model.Transaction;
import com.orange.validation.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Response> addTransaction(@RequestBody Transaction transaction) {
        Response response = transactionService.sendTransactionForProcessing(transaction);

        if(response.getErrors().isEmpty()) {
            return   new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


}
