package com.orange.validation.error;

import java.io.Serializable;
import java.util.Set;

public class Error implements Serializable {

    private String fieldName;
    private Set<RestError> restError;

    public Error(String fieldName, Set<RestError> restError) {
        this.fieldName = fieldName;
        this.restError = restError;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Set<RestError> getRestError() {
        return restError;
    }

    public void setRestError(Set<RestError> restError) {
        this.restError = restError;
    }
}
