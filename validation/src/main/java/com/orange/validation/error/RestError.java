package com.orange.validation.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum RestError implements Serializable {

    IBAN_INVALID(1, "IBAN is invalid"),
    EMPTY_VALUE(2, "value cannot be empty"),
    LESS_THAN_ZERO(3, "a value less than 0 is not allowed"),
    CNP_INVALID(4, "CNP is invalid");

    private RestError(int errorCode, String description) {
        this.errorCode = errorCode;
        this.description = description;
    }

    @JsonProperty
    public int errorCode;
    public String description;
//
//    @Override
//    @JsonValue
//    public String toString() {
//        return description;
//    }
}
