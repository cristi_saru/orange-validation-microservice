package com.orange.validation.service;

import com.orange.validation.model.Response;
import com.orange.validation.model.Transaction;

public interface TransactionService {

    Response sendTransactionForProcessing(Transaction transaction);
}
