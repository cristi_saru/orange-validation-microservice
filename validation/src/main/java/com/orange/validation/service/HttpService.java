package com.orange.validation.service;

import com.orange.validation.model.CreateTransactionResponse;
import com.orange.validation.model.Transaction;
import org.springframework.http.ResponseEntity;

public interface HttpService {

    ResponseEntity<CreateTransactionResponse> sendTransactionAsync(Transaction transaction);
}
