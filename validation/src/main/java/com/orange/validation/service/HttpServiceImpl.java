package com.orange.validation.service;

import com.orange.validation.model.CreateTransactionResponse;
import com.orange.validation.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpServiceImpl implements  HttpService {

    @Value("${persistence.port}")
    private String port;

    @Value("${persistence.uri}")
    private String uri;

    @Autowired
    RestTemplate restTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServiceImpl.class);

    @Async
    @Retryable(value = { ResourceAccessException.class }, backoff = @Backoff(delay = 100), maxAttempts = 3)
    public ResponseEntity<CreateTransactionResponse> sendTransactionAsync(Transaction transaction) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:" + port + uri;
        ResponseEntity<CreateTransactionResponse> responseEntity;

          LOGGER.info("send request");
          responseEntity = restTemplate.postForEntity(address, transaction, CreateTransactionResponse.class);
          HttpStatus status = responseEntity.getStatusCode();

        //further processing can be done here
        // this is an intermediate result since the procesing is done async
        //the final result will be received trough a kafka que

        return  responseEntity;
    }

    @Recover
    public ResponseEntity<CreateTransactionResponse> recover(Transaction transaction){
        //just a dummy recover method
        //in a real life scenario i would do the following
        //1. this microservice should have a table which will hold all the failed transactions
        //with the reason for failing and the serialized transaction
        //2. Have a pooler which at a certain amount of tine will take the transactions and try to send it to the persistent microservice
        //in this way the transactions are not lost
        //3. I would post a message to a que that the transaction has failed and inform the user or the parties interested
        //also i think it would be a good idea to post a message to the que also if the transaction succeeded after the pooler had tried
        //to resend the transaction to the persistence microservice
        LOGGER.info("Service recovering");
        CreateTransactionResponse createTransactionResponse = new CreateTransactionResponse();
        createTransactionResponse.setStatus("FAILED_TO_CONNECT_PERSISTENCE");
        //not the best solution but it's just a demo...
        return new ResponseEntity<CreateTransactionResponse>(createTransactionResponse,HttpStatus.BAD_GATEWAY);
    }

}
