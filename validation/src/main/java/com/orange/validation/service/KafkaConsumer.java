package com.orange.validation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.validation.model.CreateTransactionResponse;
import org.springframework.stereotype.Service;
import java.io.IOException;

@Service
public class KafkaConsumer {

    //@KafkaListener(topics = "${kafka.topic}")
    public void consume(String message) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        CreateTransactionResponse createTransactionResponse = mapper.readValue(message, CreateTransactionResponse.class);
        //received final status of the processing
    }
}
