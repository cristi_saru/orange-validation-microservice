package com.orange.validation.service;

import com.orange.validation.error.Error;
import com.orange.validation.model.Response;
import com.orange.validation.model.Transaction;
import com.orange.validation.validator.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private HttpServiceImpl httpServiceImpl;

    @Override
    public Response sendTransactionForProcessing(Transaction transaction) {
        Response response = new Response();
        Set<Error> errors = transactionValidator.validate(transaction);
        response.setErrors(transactionValidator.validate(transaction));

        if(errors.isEmpty()) {
            httpServiceImpl.sendTransactionAsync(transaction);
        }

        return  response;

    }
}
