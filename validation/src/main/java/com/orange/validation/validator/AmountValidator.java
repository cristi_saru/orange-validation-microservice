package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class AmountValidator implements AbstractValidator<Double> {

    private Set<RestError> errors;

    @Override
    public Set<RestError> validate(Double amount) {
        errors = new HashSet<>();

        if (BigDecimal.valueOf(amount.doubleValue()).compareTo(BigDecimal.ZERO) < 0) {
            errors = new HashSet<>();
            errors.add(RestError.LESS_THAN_ZERO);
        }
        return errors;
    }
}
