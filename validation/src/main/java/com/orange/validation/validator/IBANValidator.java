package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class IBANValidator implements AbstractValidator<String> {

    //dummy iban validator regex
    private final String regex = "\\b[A-Z]{2}[0-9]{2}(?:[ ]?[0-9]{4}){4}(?!(?:[ ]?[0-9]){3})(?:[ ]?[0-9]{1,2})?\\b";

    private Set<RestError> errors;

    @Override
    public Set<RestError> validate(String iban) {
        errors = new HashSet<>();

        if (StringUtils.isEmpty(iban)) {
            errors.add(RestError.EMPTY_VALUE);
        } else {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(iban.trim());

            if (!matcher.matches()) {
                errors.add(RestError.IBAN_INVALID);
            }
        }

        return errors;
    }
}
