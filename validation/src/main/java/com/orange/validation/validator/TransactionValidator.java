package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@Service
public class TransactionValidator {

    private Set<Error> errors;

    @Autowired
    private IBANValidator ibanValidator;

    @Autowired
    private CNPValidator cnpValidator;

    @Autowired
    private NameValidator nameValidator;

    @Autowired
    private DescriptionValidator descriptionValidator;

    @Autowired
    private AmountValidator amountValidator;

    public Set<Error> validate(Transaction transaction) {
        errors = new HashSet<>();

        Set<Error> errors = new HashSet<>();

        if(!ibanValidator.validate(transaction.getSenderIban()).isEmpty()) {
            errors.add(new Error("senderIban", ibanValidator.validate(transaction.getReceiverCnp())));
        }

        if(!cnpValidator.validate(transaction.getSenderCnp()).isEmpty()) {
            errors.add(new Error("senderCnp", cnpValidator.validate(transaction.getSenderCnp())));
        }

        if(!ibanValidator.validate(transaction.getReceiverIban()).isEmpty()) {
            errors.add(new Error("receiverIban", ibanValidator.validate(transaction.getReceiverIban())));
        }

        if(!cnpValidator.validate(transaction.getReceiverCnp()).isEmpty()) {
            errors.add(new Error("receiverCnp", cnpValidator.validate(transaction.getReceiverCnp())));
        }

        if(!descriptionValidator.validate(transaction.getDescription()).isEmpty()) {
            errors.add(new Error("description", descriptionValidator.validate(transaction.getDescription())));
        }

        if(!amountValidator.validate(transaction.getAmount()).isEmpty()) {
            errors.add(new Error("ammount", amountValidator.validate(transaction.getAmount())));
        }

        return errors;
    }
}
