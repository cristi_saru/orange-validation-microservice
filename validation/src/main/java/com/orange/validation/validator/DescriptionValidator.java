package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

@Service
public class DescriptionValidator implements AbstractValidator<String> {

    private Set<RestError> errors;

    @Override
    public Set<RestError> validate(String description) {
        errors = new HashSet<>();

        if (StringUtils.isEmpty(description)) {
            errors.add(RestError.EMPTY_VALUE);
        }

        return errors;
    }
}
