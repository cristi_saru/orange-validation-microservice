package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;

import java.util.Set;

public interface AbstractValidator<T> {

    Set<RestError> validate(T t);
}

