package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CNPValidator implements AbstractValidator<String> {

    private Set<RestError> errors;
    private final String regex = "^[1-9]\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])(0[1-9]|[1-4]\\d|5[0-2]|99)(00[1-9]|0[1-9]\\d|[1-9]\\d\\d)\\d$";

    @Override
    public Set<RestError> validate(String cnp) {
        errors = new HashSet<>();

        if (StringUtils.isEmpty(cnp)) {
            errors.add(RestError.EMPTY_VALUE);
        } else {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(cnp);

            if (!matcher.matches()) {
                errors.add( RestError.CNP_INVALID);
            }
        }

        return errors;
    }
}
