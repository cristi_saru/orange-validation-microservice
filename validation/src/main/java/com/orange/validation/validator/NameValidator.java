package com.orange.validation.validator;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

@Service
public class NameValidator implements AbstractValidator<String> {

    private Set<RestError> errors;

    @Override
    public Set<RestError> validate(String name) {
        errors = new HashSet<>();

        if (StringUtils.isEmpty(name)) {
            errors = new HashSet<>();
            errors.add(RestError.EMPTY_VALUE);
        }

        return errors;
    }
}
