package com.orange.validation;

import com.orange.validation.error.Error;
import com.orange.validation.error.RestError;
import com.orange.validation.model.Transaction;
import com.orange.validation.validator.TransactionValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class TransactionValidatorTest {

    @Autowired
    private TransactionValidator transactionValidator;

    @Test
    public void assert_invalid_sender_iban() {
        Transaction transaction = new Transaction();
        transaction.setSenderIban("AB12 12asd34 1asd234 1234 1234 1");
        transaction.setSenderCnp("5200406019517");
        transaction.setReceiverIban("AB12 1234 1234 1234 1234 1");
        transaction.setReceiverCnp("5200406019517");
        transaction.setDescription("description");
        transaction.setName("name");
        transaction.setAmount(Double.valueOf(5));

        Set<RestError> errors = new HashSet<>();
        errors.add(RestError.IBAN_INVALID);

        //check that the fieldName is present
        assertThat(transactionValidator.validate(transaction).stream()
                .filter(error -> error.getFieldName().equals("senderIban")))
                .isNotEmpty();
    }

}
