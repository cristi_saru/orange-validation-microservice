package com.orange.validation;

import com.orange.validation.error.RestError;
import com.orange.validation.validator.DescriptionValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DescriptionValidatorTest {

    @Autowired
    private DescriptionValidator descriptionValidator;

    @Test
    public void assert_empty_name() {
        String empty = "";

        assertThat(descriptionValidator.validate(empty)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public void assert_null_value() {

        assertThat(descriptionValidator.validate(null)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public void assert_valid_value() {
        String valid = "validDescription";

        assertThat(descriptionValidator.validate(valid)).isEmpty();
    }

}
