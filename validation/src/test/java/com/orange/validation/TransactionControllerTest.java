package com.orange.validation;

import com.orange.validation.model.Transaction;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
@WebAppConfiguration
public class TransactionControllerTest {


    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }



    @Test
    public void assert_create_transaction_valid_data() {
        Transaction transaction = new Transaction();
        transaction.setSenderIban("AB12 1234 1234 1234 1234 1");
        transaction.setSenderCnp("5200406019517");
        transaction.setReceiverIban("AB12 1234 1234 1234 1234 1");
        transaction.setReceiverCnp("5200406019517");
        transaction.setDescription("description");
        transaction.setName("name");
        transaction.setAmount(Double.valueOf(5));

        ResponseEntity<String> responseEntity = this.restTemplate()
                .postForEntity("http://localhost:8081"  + "/transactions/create", transaction, String.class);

        assertEquals(202, responseEntity.getStatusCodeValue());
    }
}
