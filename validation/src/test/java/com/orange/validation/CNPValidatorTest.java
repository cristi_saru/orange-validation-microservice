package com.orange.validation;

import com.orange.validation.error.RestError;
import com.orange.validation.validator.CNPValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class CNPValidatorTest {

    @Autowired
    private CNPValidator cnpValidator;

    @Test
    public void assert_cnp_is_valid_with_invalid_data() {
        String invalidCnp = "invalidCnp123";

        assertThat(cnpValidator.validate(invalidCnp)).containsOnly(RestError.CNP_INVALID);
    }

    @Test
    public void assert_cnp_empty_value() {
        String empty = "";
        assertThat(cnpValidator.validate(empty)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public void assert_cnp_null_value() {
        String empty = null;
        assertThat(cnpValidator.validate(empty)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public  void assert_cnp_is_valid_with_valid_data() {
        String validCnp = "5200406019517";

        assertThat(cnpValidator.validate(validCnp)).isEmpty();
    }

}
