package com.orange.validation;

import com.orange.validation.error.RestError;
import com.orange.validation.validator.IBANValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class IBANValidatorTest {


    @Autowired
    private IBANValidator ibanValidator;

    @Test
    public void assert_iban_is_valid_with_invalid_data() {
        String invalidIban = "invalidIban123";

        assertThat(ibanValidator.validate(invalidIban)).containsOnly(RestError.IBAN_INVALID);
    }

    @Test
    public void assert_iban_empty_value() {
        String empty = "";
        assertThat(ibanValidator.validate(empty)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public void assert_iban_null_value() {
        String empty = null;
        assertThat(ibanValidator.validate(empty)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public  void assert_iban_is_valid_with_valid_data() {
        String validIban = "AB12 1234 1234 1234 1234 1";

        assertThat(ibanValidator.validate(validIban)).isEmpty();
    }
}
