package com.orange.validation;

import com.orange.validation.model.CreateTransactionResponse;
import com.orange.validation.model.Transaction;
import com.orange.validation.model.TransactionType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class PersistenceIntegrationTest {

    @Autowired
    RestTemplate restTemplate;

    @Value("${persistence.port}")
    private String port;

    @Value("${persistence.uri}")
    private String uri;

    private final String STATUS_STARTED = "STARTED_PROCESSING";

    @Test
    public void add_transaction_and_check_response() {
        // in a real life scenario i would consider creating a builder for the test data
        //TransactionBuilder.buildTransaction().withInvalidName("").withInvalidAmount(-3.5) ...
        Transaction transaction = new Transaction();
        transaction.setSenderIban("AB12 1234 1234 1234 1234 1");
        transaction.setSenderCnp("5200406019517");
        transaction.setReceiverIban("AB12 1234 1234 1234 1234 1");
        transaction.setReceiverCnp("5200406019517");
        transaction.setDescription("description");
        transaction.setName("name");
        transaction.setTransactionType(TransactionType.WALLET_TO_WALLET);
        transaction.setAmount(Double.valueOf(5));

        CreateTransactionResponse createTransactionResponse = addTransaction(transaction).getBody();

        assertEquals("Status mismatch", STATUS_STARTED, createTransactionResponse.getStatus());
    }

    private ResponseEntity<CreateTransactionResponse> addTransaction(Transaction transaction){
        String address = "http://localhost:" + port + uri;
        ResponseEntity<CreateTransactionResponse> responseResponseEntity = restTemplate.postForEntity(address, transaction, CreateTransactionResponse.class);

        return responseResponseEntity;
    }

}
