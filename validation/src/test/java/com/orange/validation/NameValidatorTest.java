package com.orange.validation;

import com.orange.validation.error.RestError;
import com.orange.validation.validator.NameValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class NameValidatorTest {

    @Autowired
    private NameValidator nameValidator;

    @Test
    public void assert_empty_name() {
        String empty = "";

        assertThat(nameValidator.validate(empty)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public void assert_null_value() {

        assertThat(nameValidator.validate(null)).containsOnly(RestError.EMPTY_VALUE);
    }

    @Test
    public void assert_valid_value() {
        String valid = "validName";

        assertThat(nameValidator.validate(valid)).isEmpty();
    }

}
