package com.orange.validation;

import com.orange.validation.error.RestError;
import com.orange.validation.validator.AmountValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AmountValidatorTest {

    @Autowired
    private AmountValidator amountValidator;

    @Test
    public void assert_amount_negative_value() {
        Double negativeValue = Double.valueOf(-3.5);

        assertThat(amountValidator.validate(negativeValue)).containsOnly(RestError.LESS_THAN_ZERO);
    }

    @Test
    public void assert_amount_valid_value() {
        Double validValue = Double.valueOf(3.5);

        assertThat(amountValidator.validate(validValue)).isEmpty();
    }

}
